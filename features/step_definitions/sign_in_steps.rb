
When(/^we type a valid email into the Email field$/) do
  valid_email = SignInHelper.new.valid_email
  @gep.type_email(valid_email)
  @email_validity = true
end

And(/^we click the email Next button$/) do
  if @email_validity
    @gpp = @gep.click_next(@email_validity)
  else
    @gep.click_next(@email_validity)
  end
end

And(/^we type an invalid password into the Password field$/) do
  invalid_password = SignInHelper.new.invalid_password
  @gpp.type_password(invalid_password)
  @password_validity = false
end

And(/^we click the password Next button$/) do
  if @password_validity
    @gip = @gpp.click_next(@password_validity)
  else
    @gpp.click_next(@password_validity)
  end
end

Then(/^a wrong password error appears with text "([^"]*)"$/) do |password_error|
  expect(@gpp.text_of_password_error).to eq password_error
end

When(/^we type an invalid email into the Email field$/) do
  invalid_email = SignInHelper.new.invalid_email
  @gep.type_email(invalid_email)
  @email_validity = false
end

Then(/^an invalid email error appears with text "([^"]*)"$/) do |email_error|
  expect(@gep.text_of_email_error).to eq email_error
end

When(/^we successfully sign in to Gmail$/) do
  valid_email = SignInHelper.new.valid_email
  @gep.type_email(valid_email)
  @email_validity = true
  @gpp = @gep.click_next(@email_validity)
  valid_password = SignInHelper.new.valid_password(valid_email)
  @gpp.type_password(valid_password)
  @password_validity = true
  @gip = @gpp.click_next(@password_validity)
end

Then(/^the Gmail inbox appears$/) do
  expect(@gip.inbox_displayed?).to be true
end

And(/^the Gmail inbox has at least one message$/) do
  expect(@gip.inbox_list_count).to be >= 1
end

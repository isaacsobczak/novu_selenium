

Feature: Sign in to Gmail
  At NovuHealth, Gmail is an essential tool for communication and productivity. It allows for the sending of email, without which we would all surely perish.


  Background: Open the Gmail About page
    Given we open the Gmail About page
      And we click Sign In


  Scenario: Unsuccessful sign in with a valid email but invalid password
    When we type a valid email into the Email field
      And we click the email Next button
      And we type an invalid password into the Password field
      And we click the password Next button
    Then a wrong password error appears with text "Wrong password. Try again."


  Scenario: Unsuccessful sign in with an invalid email
    When we type an invalid email into the Email field
      And we click the email Next button
    Then an invalid email error appears with text "Enter a valid email or phone number"

  @smoke
  Scenario: Successful sign in to Gmail
    When we successfully sign in to Gmail
    Then the Gmail inbox appears
      And the Gmail inbox has at least one message

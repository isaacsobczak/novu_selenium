
require_relative 'page'

##
# The GmailAboutPage class represents the /about Gmail landing page
class GmailAboutPage < Page

  attr_reader :url

  SIGN_IN = { css: '.gmail-nav__nav-link.gmail-nav__nav-link__sign-in' }.freeze

  def initialize(driver)
    super driver
    @url = GMAIL_ABOUT_PAGE_URL
  end

  def click_sign_in
    click SIGN_IN
    GmailEmailPage.new(@driver)
  end
end

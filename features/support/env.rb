
require 'os'
require 'selenium-webdriver'

Before do
  if OS.mac?
    Selenium::WebDriver::Chrome.driver_path = File.join(Dir.pwd, 'resources', 'mac', 'chromedriver')
  elsif OS.windows?
    Selenium::WebDriver::Chrome.driver_path = File.join(Dir.pwd, 'resources', 'win', 'chromedriver.exe')
  elsif OS.linux?
    Selenium::WebDriver::Chrome.driver_path = File.join(Dir.pwd, 'resources', 'linux', 'chromedriver')
  else
    Selenium::WebDriver::Chrome.driver_path = File.join(Dir.pwd, 'resources', 'mac', 'chromedriver')
  end
  @driver = Selenium::WebDriver.for :chrome
  @driver.manage.window.maximize
  @page = Page.new @driver
end

After do
  @driver.quit
end


require_relative 'page'

##
# The GmailPasswordPage class represents the Google accounts page for providing a password
class GmailPasswordPage < Page

  PASSWORD       = { name: 'password' }.freeze
  NEXT           = { id: 'passwordNext' }.freeze
  PASSWORD_ERROR = { css: '.dEOOab.RxsGPe' }.freeze

  def initialize(driver)
    super driver
  end

  def next_displayed?
    displayed? NEXT
  end

  def type_password(password)
    wait_for(3) { next_displayed? }
    type_into PASSWORD, password
  end

  def click_next(password_is_valid = true)
    wait_for(3) { click_if_able? NEXT }
    GmailInboxPage.new(@driver) if password_is_valid
  end

  def password_error_displayed?
    displayed? PASSWORD_ERROR
  end

  def text_of_password_error
      wait_for(3) { password_error_displayed? }
      text_of PASSWORD_ERROR
  end
end

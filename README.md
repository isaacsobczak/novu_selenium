# Selenium, Cucumber, Gmail
A UI test automation framework using Cucumber to write test specifications, Selenium to control the web browser, and RSpec Expectations to make assertions.
## Setup
- Install Ruby
- Install Bundler: gem install bundler
- Install dependencies: bundle install
- In /features/support/sign_in_helper.rb, alter VALID_EMAIL and VALID_PASSWORD to your chosen test account credentials
## Execute tests
- Execute tests: cucumber
- Execute tests and get report: cucumber -f html --out chrome-report.html
## Assumptions
- Two-factor authentication is turned off
- The "Don't get locked out of your account" screen does not appear between submitting your password and viewing the inbox
- There will not be a captcha upon submitting an invalid password
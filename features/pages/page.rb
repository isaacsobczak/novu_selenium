
##
# The Page class is the base page, abstracting driver functionality from
# selenium-webdriver that will be used in child page classes.
class Page

  GMAIL_ABOUT_PAGE_URL  = 'https://www.google.com/gmail/about/'.freeze unless defined? GMAIL_ABOUT_PAGE_URL

  attr_reader :driver

  def initialize(driver)
    @driver = driver
  end

  def open_url(url)
    driver.get url
  end

  # Open the application landing page
  def open_gmail_about_page
    open_url GMAIL_ABOUT_PAGE_URL
    GmailAboutPage.new(@driver)
  end

  # Methods encapsulating basic Selenium functionality
  def find(locator, parent = nil)
    if !parent.nil?
      driver.find_element(parent).find_element(locator)
    else
      driver.find_element(locator)
    end
  end

  def find_all(locator, parent = nil)
    if !parent.nil?
      driver.find_element(parent).find_elements(locator)
    else
      driver.find_elements(locator)
    end
  end

  def click(locator, parent = nil)
    find(locator, parent).click
  end

  def type_into(locator, input, parent = nil, clear = false)
    find(locator, parent).clear if clear == true
    find(locator, parent).send_keys input
  end

  def rescue_exceptions
    yield
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  rescue Selenium::WebDriver::Error::StaleElementReferenceError
    false
  end

  def click_if_able?(locator, parent = nil)
    click(locator, parent)
    true
  rescue Selenium::WebDriver::Error::UnknownError
    false
  end

  def displayed?(locator, parent = nil)
    rescue_exceptions { find(locator, parent).displayed? }
  end

  def text_of(locator, parent = nil)
    find(locator, parent).text
  end

  def wait_for(seconds)
    Selenium::WebDriver::Wait.new(timeout: seconds).until { yield }
  end
end

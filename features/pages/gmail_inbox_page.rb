
require_relative 'page'

##
# The GmailInboxPage class represents the Gmail email inbox
class GmailInboxPage < Page

  INBOX  = { partial_link_text: 'Inbox' }.freeze
  EMAILS = { css: '.F.cf.zt' }.freeze
  EMAIL     = { tag_name: 'tr' }.freeze

  def initialize(driver)
    super driver
  end

  def inbox_displayed?
    wait_for(5) { displayed? INBOX }
  end

  def inbox_list_count
    inbox_list = find_all EMAIL, EMAILS
    inbox_list.size
  end
end

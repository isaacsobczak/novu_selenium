
require_relative 'page'

##
# The GmailEmailPage class represents the Google accounts page for providing an email
class GmailEmailPage < Page

  EMAIL_OR_PHONE = { name: 'identifier' }.freeze
  NEXT           = { id: 'identifierNext' }.freeze
  EMAIL_ERROR    = { css: '.dEOOab.RxsGPe' }.freeze

  def initialize(driver)
    super driver
  end

  def next_displayed?
    displayed? NEXT
  end

  def type_email(email)
    wait_for(3) { next_displayed? }
    type_into EMAIL_OR_PHONE, email
  end

  def click_next(email_is_valid = true)
    click NEXT
    GmailPasswordPage.new(@driver) if email_is_valid
  end

  def email_error_displayed?
    displayed? EMAIL_ERROR
  end

  def text_of_email_error
    wait_for(3) { email_error_displayed? }
    text_of EMAIL_ERROR
  end
end

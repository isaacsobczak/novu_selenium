
##
# The SignInHelper class provides a data store of emails, passwords, and ways to
# access them depending on the environment.
class SignInHelper

  VALID_EMAIL      = 'sautilleis@gmail.com'.freeze
  VALID_PASSWORD   = 'Z%eXd/|Y4a!YPe]k'.freeze
  INVALID_EMAIL    = 'invalid $($($($($(@^1'.freeze
  INVALID_PASSWORD = 'invalid_password'.freeze

  def valid_email
    VALID_EMAIL
  end

  def invalid_email
    INVALID_EMAIL
  end

  # Example if handling multiple accounts
  def valid_password(email)
    case email
    when VALID_EMAIL
      VALID_PASSWORD
    else
      INVALID_PASSWORD
    end
  end

  def invalid_password
    INVALID_PASSWORD
  end
end
